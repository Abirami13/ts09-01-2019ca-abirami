package assignment2;

import java.util.Scanner;

public class Pattern2 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("enter n:");
        int n=sc.nextInt();
        System.out.println("enter symbol");
        char c=sc.next().charAt(0);
        for(int i=1;i<=n;i++)
        {
        	for(int j=1;j<=n-i;j++) {
        		System.out.print(" ");
        	}
        	for(int j=1;j<=n-1;j++) {
        		System.out.print(c);
        	}
        	System.out.println();
	}
	}
}
